# QC_Project1

The aim of this project is to benchmark a QPU by using a simulated QPU on a CPU.
We will consider T1 and T2* coherence time on 5 qbits.

This project is composed by 2 file.
For its good use, the 2 files have to be in the same current directory.

The first: noise_model.py
This file contains 2 functions: t1_noise_model and t2_star_noise_model.
Each function creates a noise model to simulate in a CPU the coherence time T1 or T2* of a QPU.

The second: benchmark.py
This file contains 2 functions: T1Benchmark and T2starBenchmark.
Those functions characterise the T1 or T2* coherence time using the noise model return by the 2 functions of noise_model.py

To compare the result, the expected values of T1 and T2* coherence time in micro-seconds for each qbits are the parameter t=[70.5, 85.0, 80.0, 90.5, 77.5] of the functions in noise_model.py.
The obtained values of T1 and T2* coherence time in seconds are the output of the 2 functions in benchmark.py.
To see the obtained values, you can uncomment the last lines of benchmark.py