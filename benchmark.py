#!/usr/bin/env python
# coding: utf-8

import numpy as np
from qiskit.ignis.characterization.coherence import t1_circuits
from qiskit.ignis.characterization.coherence import t2star_circuits
from qiskit.ignis.characterization.coherence import T1Fitter
from qiskit.ignis.characterization.coherence import T2StarFitter
from qiskit import *

#--------------------------------------------------------------------------------------------------------------------

def T1Benchmark(strInput) :
    
    # Create an object named module on wich we can use the functions from noise_model.py
    
    module = __import__(strInput)

    # Create the circuit

    gate_time = 0.1
    num_of_gates = [10*i for i in range(60)]
    num_of_gates = np.array(num_of_gates)
    qubits = [i for i in range(5)]

    t1Circuits, t1_delay_times = t1_circuits(num_of_gates, gate_time, qubits)

    # Run this quantum circuit on classical computer simulating a quantum computer

    simulator = Aer.get_backend('qasm_simulator')
    t1_result = execute(t1Circuits, backend=simulator, shots=2048, noise_model=module.t1_noise_model()).result()

    # Fit the result to the lesson's courbe

    t1_fit = T1Fitter(t1_result, t1_delay_times, qubits, fit_p0=[1,15.2,1], fit_bounds=([0,0,-1], [2,110,1]))
    t1_characteristic_times = t1_fit.time()

    # Create the output

    dictOutput = {}
    for i in range(5) :
        dictOutput["Q"+str(i)] = t1_characteristic_times[i]/(10**6)
    
    return dictOutput
    
    
def T2starBenchmark(strInput) :
    
    # Create an object named module on wich we can use the functions from noise_model.py
    
    module = __import__(strInput)

    # Create the circuit

    gate_time = 0.1
    num_of_gates = [10*i for i in range(60)]
    num_of_gates = np.array(num_of_gates)
    qubits = [i for i in range(5)]

    t2StarCircuits, t2star_delay_times, osc_freq = t2star_circuits(num_of_gates, gate_time, qubits, nosc=4)

    # Run this quantum circuit on classical computer simulating a quantum computer

    simulator = Aer.get_backend('qasm_simulator')
    t2star_result = execute(t2StarCircuits, backend=simulator, shots=2048, noise_model=module.t2_star_noise_model()).result()

    # Fit the result to the lesson's courbe

    t2star_fit = T2StarFitter(t2star_result, t2star_delay_times, qubits, fit_p0=[0.5,16.3,osc_freq,0,0.5], fit_bounds=([0,0,0,-np.pi,0], [2,110,2*osc_freq,np.pi,1]))
    t2star_characteristic_times = t2star_fit.time()

    # Create the output

    dictOutput = {}
    for i in range(5) :
        dictOutput["Q"+str(i)] = t2star_characteristic_times[i]/(10**6)
    
    return dictOutput

#--------------------------------------------------------------------------------------------------------------------
    
#output = T1Benchmark("noise_model")
#print(output)

#output = T2starBenchmark("noise_model")
#print(output)